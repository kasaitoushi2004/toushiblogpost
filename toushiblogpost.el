;;; toushiblogpost --- ブログ記事を私のウェブサイトに掲載できるようHTMLのメタ情報やphp includeを加え，index.phpおよびrss.xmlに書き加える内容を生成する。現時点で，index.phpやrss.xmlに自動で書き加えることはできないが，今後実装するつもりである。
;; Author: Kasai Toushi <mi@kasaitoushi.nagano.jp>
;; URL: https://kasaitoushi.nagano.jp
;; Version: 0.1.2-J
;;; Commentary:
;;ブログ記事の<body>タグ内をHTMLで書き，M-x blogpost を実行すると，HTMLのメタ情報やphp includeが加えられる。また，ファイルの末尾に，index.phpやrss.xmlに追記すべき内容もくわえられる。
;;このプログラムは私のウェブサイトに掲載する記事用につくられた。そのため，ところどころに私のウェブサイトのURLや，私の名前が載っている。これらはご自身のウェブサイトのURLや，名前に置き換えられたい。
;;GNU一般公衆利用許諾書（GNU GPL）の第三版にもとづき利用を許諾する。詳細については，COPYINGファイルを参照のこと。

(defun blogpost (doknr title)
  "blogpost NUMERO-DE-DOKUMENTO TITOLO"
  (interactive "s文書番号を入力せよ: \ns記事の題名を入力せよ: ")
  
  (beginning-of-buffer)
  (insert "<!DOCTYPE html>\n")
  (insert "<html lang=")
  (insert-char 34)
  (insert "ja")
  (insert-char 34)
  (insert ">\n<head>\n")

  (insert "<meta http-equiv=")
  (insert-char 34)
  (insert "Content-Type")
  (insert-char 34)
  (insert " content=")
  (insert-char 34)
  (insert "text/html")
(insert-char 34)
(insert " charset=")
  (insert-char 34)
  (insert "UTF-8")
  (insert-char 34)
  (insert ">\n")

  (insert "<meta name=")
  (insert-char 34)
  (insert "author")
  (insert-char 34)
  (insert " content=")
  (insert-char 34)
  (insert "KASAI Toushi")
  (insert-char 34)
  (insert ">\n")

    (insert "<meta name=")
  (insert-char 34)
  (insert "date")
  (insert-char 34)
  (insert " content=")
  (insert-char 34)
  (insert (concat
   "" (format-time-string "%Y-%m-%d")))
  (insert-char 34)
  (insert ">\n")
  
  (insert "<link rel=")
  (insert-char 34)
  (insert "license")
  (insert-char 34)
  (insert " href=")
  (insert-char 34)
  (insert "http://creativecommons.org/licenses/by-sa/4.0/")
  (insert-char 34)
  (insert ">\n")
  
  (insert "<title>")
  (insert (format title))
  (insert "|笠井闘志のブログ</title>\n</head>\n<body>")

  (insert " <?php include ('../header.php'); include ('./header.php'); ?>\n")
  (insert "<h1>")
  (insert (format title))
  (insert "</h1>\n")
  (insert "<p>")
  (insert (concat
	   "" (format-time-string "%Y-%m-%d")))
  (insert "</p>")

  
  (end-of-buffer)
  (insert "<?php include ('../footer.php'); ?>\n</body>\n</html>")
  (insert "\n\n--- これ以降は，記事のPHPファイルに含むなかれ--\n")
  
(insert "\n-- 'rss.xml' に下記を加えよ--\n")
(insert "<item><title>")
(insert (format title))
(insert "</title><link>https://kasaitoushi.nagano.jp/blogo/")
(insert (format doknr))
  (insert "</link><guid>https://kasaitoushi.nagano.jp/blogo/")
(insert (format doknr))
(insert ".php</guid><pubDate>")
  (insert (concat
   "" (format-time-string "%Y-%m-%d")))
  (insert "</pubDate></item>\n")

  
	  (insert "\n-- 'index.php' に下記を加えよ--\n")
          (insert "<a href=")
	  (insert-char 34)
	  (insert "./")
	  (insert (format doknr))
	  	  (insert-char 34)
	  (insert ">")
	  (insert (format title))
	  (insert "</a> ")
	    (insert (concat
		     "" (format-time-string "%Y-%m-%d")))
	    (insert "<br>")
	  
	  
		  
  );
;;; toushiblogpost.el ends here